<?php

namespace App\Http\Controllers;

use App\Models\Plataforma;
use App\Http\Requests\StorePlataformaRequest;
use App\Http\Requests\UpdatePlataformaRequest;

class PlataformaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Plataforma::all();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StorePlataformaRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StorePlataformaRequest $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Plataforma  $plataforma
     * @return \Illuminate\Http\Response
     */
    public function show(Plataforma $plataforma)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Plataforma  $plataforma
     * @return \Illuminate\Http\Response
     */
    public function edit(Plataforma $plataforma)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdatePlataformaRequest  $request
     * @param  \App\Models\Plataforma  $plataforma
     * @return \Illuminate\Http\Response
     */
    public function update(UpdatePlataformaRequest $request, Plataforma $plataforma)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Plataforma  $plataforma
     * @return \Illuminate\Http\Response
     */
    public function destroy(Plataforma $plataforma)
    {
        //
    }
}
