<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PlataformaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('plataformas')->insert([
            [
                "nombre" => "Juan Perez"
            ],
            [
                "nombre" => "Ana Diaz"
            ],
            [
                "nombre" => "Sara Angulo"
            ],
            [
                "nombre" => "Juan Chambi"
            ],
        ]);
    }
}
