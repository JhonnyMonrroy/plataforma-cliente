<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class EstadoCivilSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('estado_civils')->insert([
            [
                "estado" => "Soltero",
                "codigo" => 0
            ],
            [
                "estado" => "Casado",
                "codigo" => 1
            ],
            [
                "estado" => "Divorciado",
                "codigo" => 2
            ],
            [
                "estado" => "Viudo",
                "codigo" => 3
            ]
        ]);
    }
}
