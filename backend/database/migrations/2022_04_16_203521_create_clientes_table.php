<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clientes', function (Blueprint $table) {
            $table->id();
            $table->string("ci");
            $table->string("extension");
            $table->string("nombre");
            $table->string("paterno");
            $table->string("materno");
            $table->string("direccion");
            $table->string("telefono");
            $table->string("email");

            $table->string("conyugue_nombre");
            $table->string("conyugue_paterno");
            $table->string("conyugue_materno");

            $table->unsignedBigInteger("estado_civil_id");
            $table->foreign("estado_civil_id")->references('id')->on('estado_civils');

            $table->unsignedBigInteger("plataforma_id");
            $table->foreign("plataforma_id")->references('id')->on('plataformas');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('clientes');
    }
};
